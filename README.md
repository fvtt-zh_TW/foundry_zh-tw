# foundry-core-zh-tw
Foundry VTT 核心系统正體中文化 12 版

* 有任何用詞、翻譯錯誤、本地化辭彙問題都可歡迎提出。
* 由於臺灣與香港的正體字文化不同，故另行創建了此翻譯模組。
* 香港使用者可以使用 https://foundryvtt.com/packages/foundry_zh-tw
* 銘謝任何願意協助完善的人
